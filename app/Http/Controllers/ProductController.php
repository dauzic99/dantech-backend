<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreProductRequest as ProductStoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest as ProductUpdateProductRequest;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use Exception;
use GuzzleHttp\Psr7\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::where('user_id', Auth::id())->get();
        // $data = Product::with('user')->where('user_id', Auth::id())->get(); if wanted to pass user too

        return sendResponse(ProductResource::collection($data), "Products Fetched");
    }


    public function store(ProductStoreProductRequest $request)
    {
        $imageName = '';
        if ($request->hasFile('image')) {
            // pindah file gambar
            $imageName = 'Product-' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('images/Product'), $imageName);
        }
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'stock' => $request->stock,
            'user_id' => Auth::id(),
            'image' => $imageName,

        ]);

        return sendResponse(new ProductResource($product), "Product Created");
    }

    public function show($id)
    {
        try {
            $product = Product::with('user')->findOrFail($id);
            return sendResponse(new ProductResource($product), "Product Fetched");
        } catch (Exception $e) {
            return sendError("Product Not Found", 404, $e->getMessage());
        }
    }

    public function update(ProductUpdateProductRequest $request, $id)
    {
        try {
            $product = Product::findOrFail($id);
            if (Auth::id() != $product->user_id) {
                throw new AuthorizationException(response()->json([
                    'errors' => "You Are Not Authorized",
                    'status' => true
                ], 401));
            }
            $imageName = $product->image;
            if ($request->hasFile('image')) {
                //hapus file gambar sebelumnya
                if (File::exists(public_path('images/Product/' . $product->image))) {
                    File::delete(public_path('images/Product/' . $product->image));
                }
                // pindah file gambar
                $imageName = 'Product-' . time() . '.' . $request->image->extension();
                $request->image->move(public_path('images/Product'), $imageName);
            }


            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->stock = $request->stock;
            $product->image = $imageName;

            $product->update();
            return sendResponse(new ProductResource($product), "Product Updated");
        } catch (Exception $e) {
            return sendError("Product Not Found", 404, $e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        if (File::exists(public_path('images/Product/' . $product->image))) {
            File::delete(public_path('images/Product/' . $product->image));
        }
        $product->delete();
        return sendResponse(new ProductResource($product), "Product Deleted");
    }

    public function uploadImage(Request $request, $id)
    {
        # code...
    }
}
