<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function response($user, $message)
    {
        $token = $user->createToken(str()->random(40))->plainTextToken;
        return sendResponse([
            'user' => $user,
            'token' => $token,
            'token_type' => 'Bearer'
        ], $message);
    }

    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'name' => ucwords($request->name),
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return $this->response($user, "You Have Signed Up Successfully");
    }

    public function login(LoginRequest $request)
    {
        $cred = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (!Auth::attempt($cred)) {
            return sendError(
                'Wrong Credential',
                401
            );
        }

        return $this->response(Auth::user(), "You Have Logged In Successfully");
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();

        return sendResponse([], "You Have Successfully Logged Out");
    }
}
