<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public function toArray($request)
    {
        $image = '';
        if ($this->image == null) {
            $image = asset('images/base_image.jpg');
        } else {
            $image = asset('images/Product/' . $this->image);
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'price' => $this->price,
            'stock' => $this->stock,
            'image' => $image,
            'user' => $this->whenLoaded('user'),
        ];
    }
}
