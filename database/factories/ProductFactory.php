<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'description' => $this->faker->sentence,
            'price' => $this->faker->numberBetween($min = 10000, $max = 200000),
            'stock' => $this->faker->numberBetween($min = 0, $max = 100),
            // 'image' => $this->faker->image(public_path('images/Product/'), 640, 480),
            // 'user_id' => $this->faker->numberBetween($min = 1, $max = 10),
        ];
    }
}
